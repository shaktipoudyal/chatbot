package com.springbootwebsocket.chatbot.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration

//enable websocket server
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

	/*
	 * STOMP is the messaging protocol/ standard that define rules and format for
	 * data exchange.. Web Socket takes help of STOMP to identify which user have
	 * subscribed to the channel (which users to send message to)..
	 */

	/*
	 * This method defines the WebSocket server end-point which client can use to
	 * establish connection with WebSocket server
	 */
	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		// why use withSockJS?- when web socket connection is down/ can not be
		// established, it is down graded to http and communication still continues
		registry.addEndpoint("/ws").withSockJS(); // setAllowedOrigins("*").withSockJS();
	}

	/*
	 * This method is used to route message from one client to another client.. when
	 * one client sends a chat message, it has to appear in another client's window
	 * (or group chat window)..
	 */
	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		// register/ define messages whose destination starts with /app
		// it will be routed to message handling method in controller
		// meaning that it will be mapped with method handling method that is tagged
		// with the @MessageMapping annotation
		registry.setApplicationDestinationPrefixes("/app");

		// messages whose destination start with /topics should be routed to the message
		// broker, and message broker will broadcast the messages to all connected users
		// subscribed to the topic
		registry.enableSimpleBroker("/topic");
	}

	// in this app, we are using in-memory message broker instead of MQ
}