package com.springbootwebsocket.chatbot.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.web.socket.messaging.SessionConnectedEvent;

import com.springbootwebsocket.chatbot.model.ChatMessagePOJO;
import com.springbootwebsocket.chatbot.model.ChatMessagePOJO.MessageType;

public class WebSocketEventListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketEventListener.class);

	@Autowired
	SimpMessageSendingOperations messagingTemplate;

	// log details when someone joins chat
	@EventListener
	public void handleWebSocketConnectListener(SessionConnectedEvent event) {
		LOGGER.info("Received a new web socket connection.");
	}

	@EventListener
	public void handleWebSocketDisconnectListener(SessionConnectedEvent event) {
		StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());

		String username = (String) headerAccessor.getSessionAttributes().get("username");

		if (username == null) {
			LOGGER.info("User Disconnected: " + username);

			ChatMessagePOJO chatMessage = new ChatMessagePOJO();
			chatMessage.setType(MessageType.LEAVE);
			chatMessage.setSender(username);

			messagingTemplate.convertAndSend("/topic/public", chatMessage);

		}
	}

}
