package com.springbootwebsocket.chatbot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RestController;

import com.springbootwebsocket.chatbot.model.ChatMessage;
import com.springbootwebsocket.chatbot.model.MessageModel;
import com.springbootwebsocket.chatbot.repo.UserStorage;

/**
 * 
 * @author shakti this class handles incoming msgs and send to users
 *
 */
@RestController
public class ChatboxController {

	@Autowired
	SimpMessagingTemplate messagingTemplate;

//	// for websocket to capture the request we use @Payload
//	// to get user information we can use SimpMessageHeaderAccessor
//
//	// @MessageMapping to map same url from client to server
//	@MessageMapping("/chat.register")
//
//	// we configured topic in config file
//	// specify queue (response channel and request channel based on url)
//	@SendTo("/topic/public")
//	public ChatMessage registerUser(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
//		headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
//		return chatMessage;
//	}

	// get incoming msg and handle/ send to following url
	// give registry end point, to= destination user
	@MessageMapping("/chat/{to}")
	@SendTo("/topic/public")
	public void sendMessage(@DestinationVariable String to, MessageModel message) {
		System.out.println(message + "  " + to);

		if (UserStorage.getInstance().getUsers().contains(to)) {

			// enter prefic as broker destination
			messagingTemplate.convertAndSend("/topic/messages/" + to, message);
		}
	}

}
