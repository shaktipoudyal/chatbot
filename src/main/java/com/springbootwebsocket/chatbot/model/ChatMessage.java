package com.springbootwebsocket.chatbot.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChatMessage {

	private String content;
	private String sender;

	public enum MessageType {
		CHAT, LEAVE, JOIN
	}
}