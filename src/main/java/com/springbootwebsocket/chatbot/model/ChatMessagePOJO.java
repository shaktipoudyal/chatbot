package com.springbootwebsocket.chatbot.model;

import lombok.Data;

@Data
public class ChatMessagePOJO {

	private MessageType type;
	private String content;
	private String sender;

	public enum MessageType {
		CHAT, JOIN, LEAVE
	}
}