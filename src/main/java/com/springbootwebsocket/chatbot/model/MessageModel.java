package com.springbootwebsocket.chatbot.model;

import lombok.Data;

@Data
public class MessageModel {

	private String message;
	private String fromLogin;

}
