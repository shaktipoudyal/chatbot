package com.springbootwebsocket.chatbot.repo;

import java.util.Set;

public class UserStorage {

	private static UserStorage instance;

	private Set<String> users; // will store users, using this instead of a real Repository

	private UserStorage() {

	}

	public static UserStorage getInstance() {
		if (instance == null) {
			instance = new UserStorage();
		}
		return instance;
	}

	// getters and setters

	public Set<String> getUsers() {
		return users;
	}

	public void setUsers(String userName) throws Exception {
		if (users.contains(userName)) {
			throw new Exception("Username already exists: " + userName);
		}
		users.add(userName);
	}

}
